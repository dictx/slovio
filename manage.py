import os
import sys

if __name__ == '__main__':
    SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
    sys.path.insert(0, SITE_ROOT)

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'slovnik.settings')

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
