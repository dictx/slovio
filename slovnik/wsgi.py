import os

os.system('/app/sbin/dicod --stderr')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'slovnik.settings')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
