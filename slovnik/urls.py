from django.conf import settings
from django.conf.urls import patterns
import views

urlpatterns = patterns('',
    (r'^/?$', views.main, {}, 'main'),
    (r'^s?$', views.search, {}, 'search'),
    (r'^i$', views.info, {}, 'info'),
    (r'^opensearch\.xml$', views.opensearch, {}, 'opensearch'),
)

urlpatterns += patterns('',
    (r'^(?P<path>.*)$', 'django.views.static.serve',
    {'document_root': settings.MEDIA_ROOT}),
)
