from django.conf import settings

import re

CONTENT_TYPES = 'text/css', 'text/html', 'text/plain'


class StripperMiddleware(object):
    def __init__(self):
        self.stripping = getattr(settings, 'STRIP_BLANK_LINES', True)

    def process_response(self, request, response):
        if not self.stripping:
            return response
        content_type = response.get('Content-Type', '').split(';')[0] 
        if content_type not in CONTENT_TYPES:
            return response
        try:
            response.content = re.sub(r'^\s+', '', response.content)
            response.content = re.sub(r'\n\s*\n', '\n', response.content)
        except AttributeError:
            pass
        return response
