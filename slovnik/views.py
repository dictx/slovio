from django.conf import settings
from django.shortcuts import render_to_response
from django.utils import html

import re
import logging
logger = logging.getLogger(__name__)

import dicoclient

server = 'localhost'
port = 2628
database = 'slovio'
strategy = 'dlev'


def prop(request, prop):
    return request.GET.get(prop, '').strip(' "')[:32]


def search(request):
    query = prop(request, 'q') 
    if not query:
        return main(request)

    if 'm' in request.GET:
        search = 'match'
        link = query
    else:
        search = 'define'
        link = prop(request, 'l')

    response, matches = result(search, query, link)
    if 'matches' in matches:
        response['matches'] = matches['matches'][database]
    if 'definitions' in response:
        reformat(response['definitions'])

    vals = dict(q=query, link=link, result=response)
    return render_to_response('main.html', vals)


def main(request):
    logger.info(head(request))
    return render_to_response('main.html', dict(q=''))


def info(request):
    response = result('info', database)[0]
    return render_to_response('main.html', dict(q='', result=response))


def head(request):
    keys = 'REMOTE_ADDR', 'HTTP_X_FORWARDED_FOR', 'HTTP_USER_AGENT', \
           'HTTP_ACCEPT_LANGUAGE', 'HTTP_REFERER'
    info = []
    for key in keys: info.append(request.META.get(key, '-'))
    return ' '.join(info)


def result(search, query, link=''):
    try:
        dico = dicoclient.DicoClient()
        dico.open(server, port)

        if search == 'match':
            result = dico.define(database, query)
            match = dico.match(database, strategy, query)
        elif search == 'define':
            result = dico.define(database, query)
            match = dico.match(database, strategy, link) if link else {}
        elif search == 'info':
            result = dico.show_info(query)
            match = {}
        else:
            raise Exception('No search')

    except Exception as e:
        logging.error(e)
        result = {'msg': str(e), 'error': '500'}
        match = {}

    finally:
        dico.close()

    return result, match


def reformat(definitions):
    for entry in definitions:
        entry['fmt'] = fmt(entry['desc'])


def fmt(text):
    word = re.sub(r'^([^\n]+)', r'<dt class="word">\1', html.escape(text))
    term = re.sub(r'\n(\d)\s', r'\n<dd><span class="term">\1</span>===', word)
    dscr = re.sub(r'===(\S+)', r' <span class="dscr">\1</span>===', term)
    abrv = re.sub(r'===\s+(\S)\s', r' <span class="abrv">\1</span> ', dscr)
    trim = re.sub(r'===', r'', abrv)
    return trim


def opensearch(request):
    from django.core import urlresolvers
    url_query = request.build_absolute_uri(urlresolvers.reverse('search'))
    url_media = request.build_absolute_uri(settings.MEDIA_URL)
    vals = dict(url_query=url_query, url_media=url_media)
    return render_to_response('opensearch.xml', vals, content_type='application/xml')
