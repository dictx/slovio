from django.conf import settings
from django.http import HttpResponsePermanentRedirect


class SSLifyMiddleware(object):
    def __init__(self):
        self.redirect = getattr(settings, 'SECURE_SSL_REDIRECT', False)

    def process_request(self, request):
        if self.redirect and not request.is_secure():
            host = request.get_host()
            path = "https://%s%s" % (host, request.get_full_path())
            return HttpResponsePermanentRedirect(path)
